from django.contrib import admin
from django.urls import path, include  # assurez-vous d'importer 'include'

urlpatterns = [
    path('admin/', admin.site.urls),
    path('CSA/', include('CSA.urls')),  # Remplacez 'CSA.urls' par le chemin correct si votre application a un nom différent
    path('', include('CSA.urls')),  # Redirige l'URL racine vers les URLs de l'application CSA
    # Vous pouvez ajouter d'autres chemins ici
]
