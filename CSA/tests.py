from django.test import TestCase
from .models import Task
from django.urls import reverse
from .forms import TaskForm

class TaskModelTests(TestCase):

    def test_task_creation(self):
        task = Task.objects.create(
            title="Test Task",
            description="Test Description",
            completed=False,
            priority=1
        )
        self.assertEqual(task.title, "Test Task")
        self.assertEqual(task.description, "Test Description")
        self.assertEqual(task.completed, False)
        self.assertEqual(task.priority, 1)

class TaskViewTests(TestCase):

    def test_task_list_view(self):
        response = self.client.get(reverse('task_list'))
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response, 'CSA/task_list.html')

    def test_task_create_view(self):
        response = self.client.get(reverse('task_create'))
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response, 'CSA/task_form.html')

class TaskFormTests(TestCase):

    def test_valid_form(self):
        data = {'title': 'Valid Title', 'description': 'Valid Description', 'completed': False, 'priority': 1}
        form = TaskForm(data=data)
        self.assertTrue(form.is_valid())

    def test_invalid_form(self):
        data = {}  # data vide pour tester l'échec de la validation
        form = TaskForm(data=data)
        self.assertFalse(form.is_valid())

class TaskDetailViewTests(TestCase):

    def setUp(self):
        self.task = Task.objects.create(
            title="Detail Test",
            description="Test Description for Detail",
            completed=False,
            priority=2
        )

    def test_task_detail_view_success(self):
        response = self.client.get(reverse('task_detail', args=(self.task.id,)))
        self.assertEqual(response.status_code, 200)
        self.assertContains(response, "Detail Test")
        self.assertTemplateUsed(response, 'CSA/task_detail.html')

    def test_task_detail_view_failure(self):
        # Assuming 9999 is an id that doesn't exist
        response = self.client.get(reverse('task_detail', args=(9999,)))
        self.assertEqual(response.status_code, 404)

class TaskDeleteViewTests(TestCase):

    def setUp(self):
        self.task = Task.objects.create(
            title="Delete Test",
            description="Test Description for Delete",
            completed=False,
            priority=4
        )

    def test_task_delete_view_get(self):
        response = self.client.get(reverse('task_delete', args=(self.task.id,)))
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response, 'CSA/task_confirm_delete.html')

    def test_task_delete_view_post(self):
        response = self.client.post(reverse('task_delete', args=(self.task.id,)))
        self.assertRedirects(response, reverse('task_list'))
        with self.assertRaises(Task.DoesNotExist):
            Task.objects.get(id=self.task.id)

class TaskUpdateViewTests(TestCase):

    def setUp(self):
        self.task = Task.objects.create(
            title="Update Test",
            description="Test Description for Update",
            completed=True,
            priority=3
        )

    def test_task_update_view_get(self):
        response = self.client.get(reverse('task_update', args=(self.task.id,)))
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response, 'CSA/task_form.html')

    def test_task_update_view_post_success(self):
        data = {
            'title': 'Updated Title',
            'description': 'Updated Description',
            'completed': False,
            'priority': 1
        }
        response = self.client.post(reverse('task_update', args=(self.task.id,)), data)
        self.assertRedirects(response, reverse('task_list'))
        self.task.refresh_from_db()
        self.assertEqual(self.task.title, 'Updated Title')
        
    def test_task_update_view_post_failure(self):
        data = {}  # Invalid data
        response = self.client.post(reverse('task_update', args=(self.task.id,)), data)
        self.assertEqual(response.status_code, 200)  # Should stay on form page due to errors
        self.assertTrue('form' in response.context)  # Ensure the form is in the context
        form = response.context['form']  # Get the form from the context
        self.assertTrue(form.is_bound)  # Check if the form is bound
        self.assertFalse(form.is_valid())  # Check if the form is invalid
        self.assertIn('title', form.errors)  # Check if 'title' field has errors

